@extends('layouts.default')
@section('content')
<div class="container">
    <div class="content">
        <div class="title">
        	<form method="post" enctype="multipart/form-data">
        		<label>Import products from .csv</label>
        		<input type="file" name="import" />
        		<input type="submit" value="Import" />
        	</form>
        </div>
        <div>
        	<table width="100%" cellpadding="10">
        		<tr>
        			<th align="center">ID</th>
        			<th>Title</th>
        			<th align="center">Categorie</th>
        			<th align="center">Price</th>
        		</tr>        		
        		@foreach ($products as $product)
        			<tr>
        				<td align="center">{{ $product->id }}</td>
        				<td>{{ $product->title }}</td>
        				<td data-cat="{{ $product->cat_id }}" align="center"><span class="ajax-href">{{ $product->name }}</span></td>
        				<td align="center">{{ $product->price }}</td>
        			</tr>
        		@endforeach
        	</table>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('body').click(function(){
			$('#response').remove();

		});
		$('.ajax-href').click(function(e) {
			var that = this;
			e.preventDefault();
			$('#response').remove();
			// console.log($(this).closest('td').data('cat'));
			var $loader = $('<div/>').appendTo($(that).closest('td')).addClass("loader");
			$.post('getDelivery', {'cat' : $(this).closest('td').data('cat')}, function(res){
				console.log($(that).closest('td'));
				var $response_bl = $('<div/>', {'id':'response', 'class':'list-cities'}).appendTo($(that).closest('td'));
				$loader.remove();

				$.each(res, function(i, item){
				console.log(item.city);
					$($response_bl).append("<p>"+item.city+"</p>");
				});
			},'json');
		});
	});
</script>
@stop
