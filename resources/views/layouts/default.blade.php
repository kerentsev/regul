<!DOCTYPE html>
<html>
	<head>
		<title>Test</title>
		<meta charset="utf8" />
		<meta name="_token" content="{!! csrf_token() !!}"/>
		<link rel="stylesheet" type="text/css" href="/css/main.css" />
		<script src="/js/jquery.min.js"></script>
	</head>
	<body>
		@yield('content')
	</body>
</html>