<?php namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use DB;

class ListController extends Controller {
	/**
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/*
	 * @return Response
	 */
	public function index()
	{
		$products = DB::table('products')
			->join("categories", "categories.id","=","products.cat_id")
			->select("products.*", "categories.name")
			->orderby("categories.name","products.title")
			->get();

		return view('list')->with('products', $products);
	}

	public function getCities()
	{
		$cid = @$_REQUEST['cat'];
		$pids = DB::table('products')
					->select("id")
					->where("cat_id", "=", $cid)
					->get();
		$pids = $this->getIds($pids);

		$delivery_ids = DB::table("delivery_link")
							->select("delivery_id as id")
							->whereIn("pid", $pids)
							->get();

		$delivery_ids = $this->getIds($delivery_ids);

		$cities = DB::table("delivery")
						->select("city")
						->whereIn("id", $delivery_ids)
						->get();
		return json_encode($cities);

	}

	public function import_products()
	{

		if (@$_FILES['import']['tmp_name'])
		{
			$handle = fopen($_FILES['import']['tmp_name'], "r") or die("File error!");

			$row_i = 1;
			while(($row = fgetcsv($handle)) !== false){
				if($row_i == 1) { $row_i++; continue; }
			   	$data = explode(";", $row[0]);

			   	// Check exists categorie

			   	$cat_id = DB::select("select id from categories where name = :name", ['name' => $data[1]]);

			   	if(!$cat_id){
			    	$cat_id = DB::table('categories')->insertGetId(array(
			    			'name' => $data[1],
			    		));		   		
			   	} else {	
			   		$cat_id = $cat_id[0]->id;
			   	}

			   	// Check exists product
			    $prod = DB::select("select * from products where title = :title", ['title' => $data[0]]);

			    if (!$prod) {
			    	$prod = DB::table('products')->insertGetId(array(
			    			'cat_id' => $cat_id,
			    			'title' => $data[0],
			    			'price' => $data[2],
			    		));
			    }

			    // Check exists delivery city
			    $cities = explode(',',$data[3]);

			    foreach ($cities as $city) {
			    	
			    	if(!$city) { continue; }

				    $delivery = DB::table("delivery")
		    						->where("city", $city)
		    						->select("id")
		    						->first();
	
				    if(!$delivery){
				    	$delivery = DB::table("delivery")->insertGetId(array(
				    			'city' => $city,
				    		));
	
				    } else { $delivery = $delivery->id; }

				    if($delivery && $prod){	
				    	DB::table("delivery_link")->insert(array(
				    			'pid' => $prod,
				    			'delivery_id' => $delivery,
				    		));
				    } else {
				    	echo "<p>Error row: ".implode(";", $data)."</p>";
				    }
			    }

			    $row_i++;
			}
			 
			fclose($handle);
		}
		return;
	}

	function getIds($ArrayObj){
			$ids = [];
			foreach ($ArrayObj as $item) {
				array_push($ids, $item->id);
			}

			return $ids;
	}


}