<?php

namespace App;

use Illuminate\Foundation\Auth\Product as Authenticatable;

class Product extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'cat_id', 'delivery_id', 'price',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'remember_token',
    // ];
}
